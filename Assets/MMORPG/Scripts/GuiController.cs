﻿using UnityEngine;
using System.Collections;

public class GuiController : Photon.MonoBehaviour {

	private UIProgressBar myHealthBar;
	private UILabel myHealthLabel;

	private UIProgressBar myManaBar;
	private UILabel myManaLabel;

	private UIProgressBar enemyHealthBar;
	private UILabel enemyHealthLabel;

	private GameObject enemyPanel;
	private GameObject spellPanel;
	private GameObject myPanel;

	private PlayerAI player;
	private MagicAI magic;

	void Start () 
	{
		if (photonView.isMine)
		{
			myHealthBar = (UIProgressBar)GameObject.Find("My Progress Bar").GetComponent("UIProgressBar");
			myHealthLabel = (UILabel)GameObject.Find("my_health_label").GetComponent("UILabel");

			myManaBar = (UIProgressBar)GameObject.Find("My Mana Bar").GetComponent("UIProgressBar");
			myManaLabel = (UILabel)GameObject.Find("my_mana_label").GetComponent("UILabel");
			
			enemyHealthBar = (UIProgressBar)GameObject.Find("Enemy Progress Bar").GetComponent("UIProgressBar");
			enemyHealthLabel = (UILabel)GameObject.Find("enemy_health_label").GetComponent("UILabel");
			
			enemyPanel = GameObject.Find("Anchor_TopRight");
			spellPanel = GameObject.Find("Anchor_BottomRight");
			myPanel = GameObject.Find("Anchor_TopLeft");

			player = (PlayerAI)gameObject.GetComponent("PlayerAI");
			magic = (MagicAI)gameObject.GetComponent("MagicAI");

			setSpellIcon();
		}
	}


	public void setBaffIcon(ArrayList arr)
	{
		GameObject [] baffs = GameObject.FindGameObjectsWithTag("Baffs");
		foreach (GameObject go in baffs)
		{
			if (go) GameObject.Destroy(go);
		}

		for (int i=0;i<arr.Count;i++)
		{
			GameObject icon = (GameObject)Instantiate(Resources.Load("gui/baffs_avatar"));
			icon.transform.parent = myPanel.transform;
			Vector3 pos = new Vector3(29.7f,-89.5f-i*42.9f,0);
			icon.transform.localPosition = pos;
			icon.transform.localScale = new Vector3(1,1,1);
			UISprite sp = (UISprite)icon.GetComponent("UISprite");
			sp.spriteName = (string)arr[i];
			icon.tag = "Baffs";
		}
		Debug.Log(arr.Count);
	}

	void setSpellIcon()
	{
		for (int i=0;i<magic.active_magics.Length;i++)
		{
			GameObject spellBtn = (GameObject)Instantiate(Resources.Load("gui/skill_btn"));
			spellBtn.transform.parent = spellPanel.transform;
			Vector3 pos = new Vector3(-42.38f,146.2f+i*78,0);
			spellBtn.transform.localPosition = pos;
			spellBtn.transform.localScale = new Vector3(1,1,1);
			MagicBtn btn = (MagicBtn)spellBtn.GetComponent("MagicBtn");
			btn.setBack(magic.active_magics[i]);
		}
	}
	// Update is called once per frame
	void Update () {
	
	}

	public void setMyProgress(float health,float max_health)
	{
		if (photonView.isMine)
		{
			if (health < 0)
			{
				health = 0;
				photonView.RPC("onDead",PhotonTargets.All);
			}
			float val = health/max_health;
			myHealthBar.value = val;
			myHealthLabel.text = Mathf.RoundToInt(health).ToString()+" / "+max_health.ToString();
		}

	}
	public void setMyMana(float mana,float max_mana)
	{
		if (photonView.isMine)
		{
			if (mana < 0)
			{
				mana = 0;
			}
			float val = mana/max_mana;
			myManaBar.value = val;
			myManaLabel.text = Mathf.RoundToInt(mana).ToString()+" / "+max_mana.ToString();
			Debug.Log("setMana");
		}
		
	}
	public void setEnemyProgress(PlayerAI enemy)
	{
		if (photonView.isMine)
		{
			showEnemyPanel();
			if (enemy.current_health < 0) enemy.current_health = 0;
			float val = enemy.current_health/enemy.health_range;
			enemyHealthBar.value = val;
			enemyHealthLabel.text = Mathf.RoundToInt(enemy.current_health).ToString()+" / "+enemy.health_range.ToString();
		}

	}
	void showEnemyPanel()
	{
		enemyPanel.SetActive(true);
	}
	void hideEnemyPanel()
	{
		enemyPanel.SetActive(false);
	}
}
