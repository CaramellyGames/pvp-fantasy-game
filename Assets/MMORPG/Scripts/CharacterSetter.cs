﻿using UnityEngine;
using System.Collections;

public class CharacterSetter : MonoBehaviour {

	public GameObject [] characters;
	private string currentCharacter;
	public GameObject preloader;

	void Start () 
	{
		preloader.SetActive(false);
		onBersek();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void onBersek()
	{
		currentCharacter = "character_bersek";
		GameServer.characterType = currentCharacter;
		sortCharacters();
	}
	public void onTank()
	{
		currentCharacter = "character_tank";
		GameServer.characterType = currentCharacter;
		sortCharacters();
	}
	public void onMage()
	{
		currentCharacter = "character_mage";
		GameServer.characterType = currentCharacter;
		sortCharacters();
	}
	public void onPriest()
	{
		currentCharacter = "character_priest";
		GameServer.characterType = currentCharacter;
		sortCharacters();
	}

	void sortCharacters ()
	{
		foreach (GameObject go in characters)
		{
			if (currentCharacter == go.name)
			{
				go.SetActive(true);
			}
			else
			{
				go.SetActive(false);
			}
		}
	}

	public void onGo()
	{
		GameObject.Find("server").SendMessage("onStartGame");
		preloader.SetActive(true);
	}
}
