﻿using UnityEngine;
using System.Collections;

public class Global : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static float calculateDamage(float attack,float min,float max)
	{
		return Random.Range(min,max)*attack;
	}
	public static GameObject getEnemy(string enemy_tag,Transform tr,GameObject me)
	{
		GameObject target = null;
		GameObject [] arr = GameObject.FindGameObjectsWithTag(enemy_tag);
		foreach (GameObject go in arr)
		{
			if (go.tag == enemy_tag && go != me)
			{
				float dis = Vector3.Distance(tr.position,go.transform.position);
				if (dis < 3)
				{
					target = go;
				}
			}
		}
		return target;
	}


}
