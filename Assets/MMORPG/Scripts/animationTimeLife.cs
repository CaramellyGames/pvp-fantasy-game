﻿using UnityEngine;
using System.Collections;

public class animationTimeLife : MonoBehaviour {

	public float timeLife = 3;

	void Start () 
	{
		Invoke("endAnim",timeLife);
	}
	
	void endAnim()
	{
		GameObject.Destroy(gameObject);
	}
}
