﻿using UnityEngine;
using System.Collections;

public class MagicBtn : MonoBehaviour {

	[HideInInspector] public string type = "rage";
	public UISprite back;
	public UILabel txt;
	public GameObject hide;
	private bool isBlock = false;
	private MagicAI magic;
	private PlayerAI player;
	private int currentCoolDown = 0;

	void Start () 
	{
		magic = (MagicAI)GameObject.Find("Player").GetComponent("MagicAI");
		player = (PlayerAI)GameObject.Find("Player").GetComponent("PlayerAI");
		txt.gameObject.SetActive(false);
		hide.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void setBack(string pic)
	{
		back.spriteName = pic;
		type = pic;
	}

	public void activeMagic()
	{
		if (isBlock == false && player.blockMoving == false)
		{
			if (magic.castSpell(type) == "active")
			{
				isBlock = true;
				txt.gameObject.SetActive(true);
				hide.SetActive(true);
				currentCoolDown = magic.getMagicCoolDown(type);
				Debug.Log(type);
				onCoolDown();
			}
		}
	}

	void onCoolDown()
	{
		if (currentCoolDown <= 0)
		{
			isBlock = false;
			txt.gameObject.SetActive(false);
			hide.SetActive(false);
		}
		else
		{
			currentCoolDown--;
			txt.text = currentCoolDown.ToString();
			Invoke("onCoolDown",1);
		}
	}
}
