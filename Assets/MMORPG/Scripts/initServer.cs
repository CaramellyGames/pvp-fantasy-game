﻿using UnityEngine;
using System.Collections;

public class initServer : MonoBehaviour {

	bool connected = false;
	bool jumpRooms;
	[SerializeField] Transform playerTransform;
	string roomName = "Super Forest";
	bool firstStart;

	bool fotRoom = false;
	bool receivedRoomsList = false;

	public GameObject [] pointArray;

	private bool isStartGame = false;

	void Start () 
	{
	
	}

	void onStartGame()
	{
		isStartGame = true;
	}

	// Update is called once per frame
	void Update () 
	{
		if (isStartGame)
		{
			if (!connected)
			{
				if (firstStart && PhotonNetwork.connected)
				{
					
				}
				if (PhotonNetwork.connectionStateDetailed.ToString() == "JoinedLobby")
				{
					connected = true;
				}
				else if (PhotonNetwork.connectionStateDetailed.ToString() == "PeerCreated" ||
				         PhotonNetwork.connectionStateDetailed.ToString() == "Disconnected")
				{
					PhotonNetwork.ConnectUsingSettings("alpha p0.1");
				}
			}
		}
	}

	void OnReceivedRoomListUpdate()
	{
		Debug.Log("on Recived");
		receivedRoomsList = true;
		foreach (var room in PhotonNetwork.GetRoomList())
		{
			roomName = room.name;
			fotRoom = true;
			break;
		}
		if (fotRoom)
		{
			PhotonNetwork.JoinRoom(roomName);
			Debug.Log("Joined room");
		}
		else
		{
			PhotonNetwork.CreateRoom(roomName,true,true,20);
			Debug.Log("Created room");
		}
	}

	void OnCreatedRoom()
	{
		Debug.Log("onRoomCreated");
	}
	void OnJoinedRoom()
	{
		Debug.Log("Player Created");
		PhotonNetwork.LoadLevel(1);
		Destroy(this);
	}

}
