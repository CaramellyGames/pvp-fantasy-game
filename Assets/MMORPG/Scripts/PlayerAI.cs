﻿using UnityEngine;
using System.Collections;

public class PlayerAI : Photon.MonoBehaviour {

	private CharacterController controller;
	[HideInInspector] public GuiController gui;
	[HideInInspector] public MagicAI magic;
	private float gravity = 0;
	[SerializeField] private Vector3 cameraPosition;
	public Transform attackPoint;

	private Vector3 correctPlayerPos = Vector3.zero;
	private Quaternion correctPlayerRot = Quaternion.identity;
	[HideInInspector] public string currentAnimation = "Idle";

	public string idle;
	public string run;
	public string death;
	public string [] attack;
	public string [] cast;

	private bool isInAttack = false;
	private bool isFindAttackTarget = false;
	[HideInInspector] public bool blockMoving = false;
	[HideInInspector] public bool isDead = false;
	[HideInInspector] public bool isFelowToTarget = false;
	[HideInInspector] public GameObject focusTarget;

	public float attack_range = 10;
	public float health_range = 800;
	public float mana_range = 200;
	public float min_damage_range = 2;
	public float max_damage_range = 5;
	public float defense_range = 10;
	public float attack_speed = 1;
	public float move_speed = 1;
	public float intellect_range = 5;

	public float visual_speed = 5;

	[HideInInspector] public float current_health;
	[HideInInspector] public float current_mana;



	void Start () 
	{
		gameObject.tag = "Player";
		current_health = health_range;
		current_mana = mana_range;
		currentAnimation = idle;
		controller = (CharacterController)gameObject.GetComponent("CharacterController");
		gui = (GuiController)gameObject.GetComponent("GuiController");
		magic = (MagicAI)gameObject.GetComponent("MagicAI");
		if (photonView.isMine) Invoke("setMyProgress",0.5f);

		EasyTouch.On_SimpleTap += OnFocusPlayer;

		if (gameObject.name != "Player")
		{
			photonView.RPC("letMeNoEnemyStats",PhotonTargets.All,current_health,defense_range);
		}
	}


	[RPC] 
	void letMeNoEnemyStats(float health,float def,PhotonMessageInfo info)
	{
		current_health = health;
		defense_range = def;
	}

	void OnDisable(){
		EasyTouch.On_SimpleTap -= OnFocusPlayer;
	}
	
	void OnDestroy(){
		EasyTouch.On_SimpleTap -= OnFocusPlayer;
	}

	[RPC]
	public void changeParameter(string type,float val,PhotonMessageInfo info)
	{
		if (type == "attack")
		{
			attack_range+=val;
		}
		if (type == "attack_speed")
		{
			attack_speed*=val;
			foreach (string anim in attack)
			{
				animation[anim].speed = attack_speed;
			}
		}
		if (type == "move_speed")
		{
			move_speed*=val;
			animation[run].speed = move_speed;
			if (photonView.isMine) SendMessage("setMoveSpeed");
		}
		if (type == "defense")
		{
			defense_range+=val;
			if (defense_range < 0)
			{
				defense_range = 0;
			}
		}
	}

	void setCameraToPosition()
	{
		GameObject cam = GameObject.Find("Camera");
		cam.transform.parent = transform;
		cam.transform.localPosition = cameraPosition;
		cam.transform.localEulerAngles = new Vector3(25.62f,0,0);

	}

	void setMyProgress()
	{
		gui.setMyProgress(current_health,health_range);
		gui.setMyMana(current_mana,mana_range);
		magic.unfocusPlayer(focusTarget);

		foreach (string anim in attack)
		{
			animation[anim].speed = attack_speed;
		}
		animation[run].speed = move_speed;
	}

	// Update is called once per frame
	void Update () 
	{
		if (!photonView.isMine && isDead == false)	
		{
			transform.position = Vector3.Lerp(transform.position, correctPlayerPos, Time.deltaTime * visual_speed);
			transform.rotation = Quaternion.Lerp(transform.rotation, correctPlayerRot, Time.deltaTime * visual_speed);
			if (!animation.IsPlaying(currentAnimation))
			{
				animation.CrossFade(currentAnimation);
			}

		}
		else
		{
			if (isInAttack)
			{
				if (isFindAttackTarget == false)
				{
					focusTarget = Global.getEnemy("Player",attackPoint,gameObject);
					if (focusTarget)
					{
						isFindAttackTarget = true;
					}
				}
			}
			if (isFelowToTarget && focusTarget != null)
			{
				float dis = Vector3.Distance(transform.position,focusTarget.transform.position);
				if (dis > 2)
				{
					transform.position = Vector3.Lerp(transform.position, focusTarget.transform.position, Time.deltaTime * visual_speed/5);
					//transform.LookAt(focusTarget.transform.position);
					//Quaternion rot = focusTarget.transform.rotation;
					//rot.y = -rot.y;
					//transform.rotation = Quaternion.Lerp(transform.rotation,rot, Time.deltaTime * visual_speed);
				}
				transform.rotation = Quaternion.LookRotation(focusTarget.transform.position - transform.position);
			}
		}
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)	
	{
		if (stream.isWriting)	
		{
			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation);
			stream.SendNext(currentAnimation);
		}
		else 
		{
			correctPlayerPos = (Vector3)stream.ReceiveNext();
			correctPlayerRot = (Quaternion)stream.ReceiveNext();
			currentAnimation = (string)stream.ReceiveNext();
		}	
	}

	public void onAttack()
	{
		if (isInAttack == false && blockMoving == false)
		{
			isFindAttackTarget = false;
			isInAttack = true;
			if (photonView.isMine) SendMessage("BlockMove");
			int randomAttack = Random.Range(0,attack.Length-1);
			animation.CrossFade(attack[randomAttack]);
			currentAnimation = attack[randomAttack];
			Invoke("onSetDamage",animation[attack[randomAttack]].length/1.4f/attack_speed);
		}
	}

	public void onSetMagicDamage(float damage)
	{
		if (focusTarget)
		{
			PlayerAI enemy_player = (PlayerAI)focusTarget.GetComponent("PlayerAI");
			float enemy_def = enemy_player.defense_range;
			//damage = damage - damage/100*enemy_def;
			if (enemy_player.current_health > 0)
			{
				if (enemy_player.current_health - damage <= 0)
				{
					magic.unfocusPlayer(focusTarget);
				}
				else
				{
					magic.focusPlayer(focusTarget);
				}
				enemy_player.photonView.RPC("onGetDamage",PhotonTargets.AllBuffered,damage);
				gui.setEnemyProgress(enemy_player);
				showDamage(damage,focusTarget.transform,false);
			}
		}
	}

	void onSetDamage()
	{
		isFindAttackTarget = false;
		animation.CrossFade(idle);
		currentAnimation = idle;
		isInAttack = false;
		if (photonView.isMine) SendMessage("UnblockMove");
		if (focusTarget)
		{
			PlayerAI enemy_player = (PlayerAI)focusTarget.GetComponent("PlayerAI");
			float enemy_def = enemy_player.defense_range;
			float damage = Global.calculateDamage(attack_range,min_damage_range,max_damage_range);
			damage = damage - damage/100*enemy_def;
			if (enemy_player.current_health > 0)
			{
				if (enemy_player.current_health - damage <= 0)
				{
					magic.unfocusPlayer(focusTarget);
				}
				else
				{
					magic.focusPlayer(focusTarget);
				}
				enemy_player.photonView.RPC("onGetDamage",PhotonTargets.AllBuffered,damage);
				gui.setEnemyProgress(enemy_player);
				showDamage(damage,focusTarget.transform,false);
			}
		}
	}
	[RPC]
	void onGetDamage (float damage,PhotonMessageInfo info)
	{
		current_health-=damage;
		gui.setMyProgress(current_health,health_range);
		if (photonView.isMine) showDamage(damage,transform,true);

	}



	void showDamage(float dam,Transform target,bool me)
	{
		int damage = Mathf.FloorToInt(dam);
		GameObject label = (GameObject)Instantiate(Resources.Load("gui/DamageLabel"));
		UILabel lb = (UILabel)label.GetComponent("UILabel");
		if (me)
		{
			lb.color = Color.red;
		}
		else
		{
			lb.color = Color.white;
		}
		lb.text = damage.ToString();
		Vector2 screenPos = Camera.main.WorldToScreenPoint(target.position);
		lb.transform.position = Camera.main.ScreenToWorldPoint(screenPos);
		iTween.MoveBy(label,iTween.Hash("y",1,"time",1,"easetype","linear","islocal",true,"oncomplete","des","oncompletetarget",gameObject,"oncompleteparams",label));
	}
	void des(GameObject label)
	{
		Debug.Log("destroyLabel");
		GameObject.Destroy(label);
	}

	private void OnFocusPlayer(Gesture gesture)
	{
		if (gesture.pickObject == gameObject)
		{
			if (photonView.isMine)
			{
				if (blockMoving == false)
				{
					focusTarget = gameObject;
					magic.focusPlayer(focusTarget);
					PlayerAI enemy_player = (PlayerAI)focusTarget.GetComponent("PlayerAI");
					gui.setEnemyProgress(enemy_player);
				}
			}
			else
			{
				GameObject realPlayer = GameObject.Find("Player");
				if (realPlayer)
				{
					PlayerAI myai = (PlayerAI)realPlayer.GetComponent("PlayerAI");
					if (myai)
					{
						GuiController myGui = (GuiController)realPlayer.GetComponent("GuiController"); 
						MagicAI mymagic = (MagicAI)realPlayer.GetComponent("MagicAI");
						mymagic.focusPlayer(gameObject);
						myGui.setEnemyProgress(this);
					}
				}

			}
		}
	}
	[RPC]
	void onDead(PhotonMessageInfo info)
	{
		isDead = true;
		if (photonView.isMine)
		{
			SendMessage("BlockMove");
			magic.unfocusPlayer(focusTarget);
		}
		animation.CrossFade(death);
		currentAnimation = death;
		StartCoroutine(onResurrectBegin());
	}

	IEnumerator onResurrectBegin()
	{
		yield return new WaitForSeconds(3);
		photonView.RPC("onResurrect",PhotonTargets.AllBuffered);
	}

	[RPC]
	void onResurrect(PhotonMessageInfo info)
	{
		isDead = false;
		isInAttack = false;
		if (photonView.isMine)
		{
			SendMessage("UnblockMove");
		}
		animation.Play(idle);
		currentAnimation = idle;
		if (photonView.isMine) GameObject.Find("server").SendMessage("setToStartPosition");
		current_health = health_range;
		current_mana = mana_range;
		if (photonView.isMine) setMyProgress();
		if (photonView.isMine) gui.setMyMana(current_mana,mana_range);
	}

	public bool checkMana(float val)
	{
		if (val >= current_mana)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public void changeMana(float val)
	{
		current_mana+=val;
		gui.setMyMana(current_mana,mana_range);
	}
	//stan
	[RPC]
	public void stunPlayer(float time,PhotonMessageInfo info)
	{

		if (photonView.isMine)
		{
			StopCoroutine("inStunning");
			SendMessage("BlockMove");
			magic.setBaff("stunning","onSimpleEnd");
		}
			
		CancelInvoke();
		magic.CancelInvoke();
		animation.CrossFade(idle);
		currentAnimation = idle;
		StartCoroutine("inStunning",time);
	}

	IEnumerator inStunning(float time)
	{
		yield return new WaitForSeconds(time);
		unstunPlayer();
		magic.endBaff("stunning");
	}


	public void unstunPlayer()
	{
		if (photonView.isMine) SendMessage("UnblockMove");
		isInAttack = false;
	}

	//freeze
	[RPC]
	public void freezePlayer(float time,float val,PhotonMessageInfo info)
	{
		if (magic.baffes.Contains("freeze"))
		{
			StopCoroutine("inFreezing");
			photonView.RPC("changeParameter",PhotonTargets.AllBuffered,"move_speed",1+val);
		}
		magic.setBaff("freeze","onSimpleEnd");
		changeParameter("move_speed",val,null);
		ArrayList param = new ArrayList();
		param.Add(time);
		param.Add(val);
		StartCoroutine("inFreezing",param);
	}
	
	IEnumerator inFreezing(ArrayList par)
	{
		yield return new WaitForSeconds((float)par[0]);
		photonView.RPC("unfreezePlayer",PhotonTargets.AllBuffered,(float)par[1]);
	}
	
	[RPC]
	public void unfreezePlayer(float val,PhotonMessageInfo info)
	{
		changeParameter("move_speed",1+val,null);
		magic.endBaff("freeze");
	}

	//take_attack
	[RPC]
	public void takeAttackPlayer(float time,float val,PhotonMessageInfo info)
	{
		if (magic.baffes.Contains("take_attack"))
		{
			StopCoroutine("inAttackTaking");
			changeParameter("attack",val,null);
		}
		changeParameter("attack",-val,null);
		magic.setBaff("take_attack","onSimpleEnd");
		ArrayList param = new ArrayList();
		param.Add(time);
		param.Add(val);
		StartCoroutine("inAttackTaking",param);
	}
	
	IEnumerator inAttackTaking(ArrayList par)
	{
		yield return new WaitForSeconds((float)par[0]);
		photonView.RPC("untakingPlayer",PhotonTargets.AllBuffered,(float)par[1]);
	}
	
	[RPC]
	public void untakingPlayer(float val,PhotonMessageInfo info)
	{
		changeParameter("attack",val,null);
		magic.endBaff("take_attack");
	}
	//health
	[RPC]
	public void onHealthPlayer(float val,PhotonMessageInfo info)
	{
		if (isDead == false)
		{
			current_health+=val;
			if (current_health > health_range) current_health = health_range;
			if (photonView.isMine)
			{
				if (focusTarget)
				{
					PlayerAI pl = (PlayerAI)focusTarget.GetComponent("PlayerAI");
					gui.setEnemyProgress(pl);
				}
				gui.setMyProgress(current_health,health_range);
			}
			else
			{
				PlayerAI ai = (PlayerAI)GameObject.Find("Player").GetComponent("PlayerAI");
				if (ai.focusTarget  = gameObject)
				{
					ai.gui.setEnemyProgress(this);
				}
			}
		}

	}

}
