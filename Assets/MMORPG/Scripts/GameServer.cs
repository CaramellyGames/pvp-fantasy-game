﻿using UnityEngine;
using System.Collections;

public class GameServer : MonoBehaviour {

	public static string characterType = "character_bersek";
	public GameObject [] pointArray;
	private GameObject player;

	void Start ()
	{
		createPlayer();
	}

	void createPlayer()
	{
		Transform randomTransform = (pointArray[Mathf.FloorToInt(Random.Range(0,pointArray.Length))].transform);
		player = PhotonNetwork.Instantiate(characterType,randomTransform.position,Quaternion.identity,0);
		//GameObject player = (GameObject)Instantiate(Resources.Load("character_test"));
		//player.transform.position = randomTransform.position;


		player.name = "Player";
		player.tag = "Player";
		player.SendMessage("setCameraToPosition");

		PlayerAI ai = (PlayerAI)player.GetComponent("PlayerAI");
		GameManager game_manager = (GameManager)GameObject.Find("gameManager").GetComponent("GameManager");
		game_manager.player = ai;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void setToStartPosition()
	{
		Transform randomTransform = (pointArray[Mathf.FloorToInt(Random.Range(0,pointArray.Length))].transform);
		player.transform.position = randomTransform.position;
	}
}
