using UnityEngine;
using System.Collections;

/// <summary>
/// Joystick event
/// </summary>
public class JoystickEvent : Photon.MonoBehaviour {

	private PlayerAI player;

	private EasyJoystick jo;

	void OnEnable()
	{
		if (photonView.isMine)
		{
			EasyJoystick.On_JoystickMove += On_JoystickMove;
			EasyJoystick.On_JoystickMoveEnd += On_JoystickMoveEnd;
			jo = (EasyJoystick)GameObject.Find("Move_Turn_Joystick").GetComponent("EasyJoystick");
			jo.XAxisTransform = gameObject.transform;
			jo.YAxisTransform = gameObject.transform;
			jo.yAxisGravity = 5;

			player = (PlayerAI)gameObject.GetComponent("PlayerAI");

			jo.speed.y = player.move_speed*10;
		}
	}

	void setMoveSpeed()
	{
		jo.speed.y = player.move_speed*10;
	}

	void OnDisable(){
		if (photonView.isMine)
		{
			EasyJoystick.On_JoystickMove -= On_JoystickMove	;
			EasyJoystick.On_JoystickMoveEnd -= On_JoystickMoveEnd;
		}
	}
		
	void OnDestroy(){
		if (photonView.isMine)
		{
			EasyJoystick.On_JoystickMove -= On_JoystickMove;	
			EasyJoystick.On_JoystickMoveEnd -= On_JoystickMoveEnd;
		}
	}
	
	
	void On_JoystickMoveEnd(MovingJoystick move){
		if (photonView.isMine && player.blockMoving == false)
		{
			if (move.joystickName == "Move_Turn_Joystick"){
				animation.CrossFade(player.idle);
				player.currentAnimation = player.idle;
			}
		}

	}
	void On_JoystickMove( MovingJoystick move){
		
		if (photonView.isMine && player.blockMoving == false)
		{
			if (move.joystickName == "Move_Turn_Joystick"){
				animation.CrossFade(player.run);
				player.currentAnimation = player.run;
			}
		}
	}


	void BlockMove()
	{
		Debug.Log("block_move");
		player.blockMoving = true;
		animation.Stop();
		jo.speed.y = 0;
		jo.speed.x = 0;
	}
	void UnblockMove()
	{
		player.blockMoving = false;
		jo.speed.y = player.move_speed*10;
		jo.speed.x = 100;
	}
	
}
