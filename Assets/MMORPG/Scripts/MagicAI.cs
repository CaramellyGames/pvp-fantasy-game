﻿using UnityEngine;
using System.Collections;
//using System;

public class MagicAI : Photon.MonoBehaviour {

	private GameObject focusAura;
	public string [] active_magics;
	private PlayerAI player;

	//rage defination
	public float rage_time = 7;
	public float rage_attack_range = 5f;
	public float rage_attack_speed_range = 2;
	//critical_attack
	public float critical_attack_distanse = 5;
	public float critical_attack_multiply = 3;
	//spurt
	public float spurt_range = 1.5f;
	public float spurt_time = 10;
	//stunning
	public float stunning_attack_distanse = 5;
	public float stunning_attack_multiply = 2;
	public float stunning_stun_time = 3;
	//crash
	public float crash_attack_distanse = 5;
	public float crash_attack_multiply = 1;
	//tank shield
	public float tank_shield_time = 30;
	public float tank_shield_defense = 25;
	//fireball
	public float fireball_damage = 10;
	public float fireball_distanse = 40;
	//freeze
	public float freeze_damage = 20;
	public float freeze_distanse = 40;
	public float freeze_time = 7;
	public float freeze_value = 0.5f;
	//take_attack
	public float take_attack_distanse = 40;
	public float take_attack_valuse = 5;
	public float take_attack_time = 10;
	//wrath_of_nature
	public float wrath_of_nature_damage = 20;
	public float wrath_of_nature_distanse = 40;
	//nature_protection
	public float nature_protection_distanse = 40;
	public float nature_protection_value = 20;
	public float nature_protection_time = 30;
	//health
	public float health_distanse = 40;
	public float health_value = 170;

	private GameObject magicTarget;

	public ArrayList baffes = new ArrayList();

	void Start () 
	{
		if (photonView.isMine)
		{
			focusAura = (GameObject)Instantiate(Resources.Load("magic/focus_aura"));
			focusAura.SetActive(false);
		}
		player = (PlayerAI)gameObject.GetComponent("PlayerAI");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void focusPlayer(GameObject target)
	{
		SendMessage("showEnemyPanel");
		focusAura.SetActive(true);
		focusAura.transform.position = target.transform.position;
		focusAura.transform.parent = target.transform;
		player.focusTarget = target;
	}
	public void unfocusPlayer(GameObject target)
	{
		target = null;
		focusAura.SetActive(false);
		SendMessage("hideEnemyPanel");
	}

	public string castSpell(string type)
	{
		string msg = "no target";
		if (photonView.isMine)
		{
			//rage
			if (type == "rage")
			{
				if (player.checkMana(getMagicMana(type)))
				{
					msg = "no mana";
					Debug.Log(getMagicMana(type));
				}
				else
				{
					msg = "active";
					onRage();
					player.changeMana(-getMagicMana(type));
				}
			}
			//critical_attack
			if (type == "critical_attack")
			{
				Debug.Log("critical");
				if (player.focusTarget == null || player.focusTarget ==gameObject)
				{
					msg = "no target";
				}
				else if (player.checkMana(getMagicMana(type))) 
				{
					msg = "no mana";
				}
				else
				{
					float dis = Vector3.Distance(transform.position,player.focusTarget.transform.position);
					Debug.Log("dis="+dis);
					if (dis < critical_attack_distanse)
					{
						msg = "active";
						onCriticalAttack();
						player.changeMana(-getMagicMana(type));
					}
					else
					{
						msg = "no distanse";
					}
				}
			}
			//spurt
			if (type == "spurt")
			{
				if (player.checkMana(getMagicMana(type)))
				{
					msg = "no mana";
				}
				else
				{
					msg = "active";
					onSpurt();
					player.changeMana(-getMagicMana(type));
				}
			}
			//stunning
			if (type == "stunning")
			{
				Debug.Log("stunning");
				if (player.focusTarget == null || player.focusTarget ==gameObject)
				{
					msg = "no target";
				}
				else if (player.checkMana(getMagicMana(type))) 
				{
					msg = "no mana";
				}
				else
				{
					float dis = Vector3.Distance(transform.position,player.focusTarget.transform.position);
					if (dis < stunning_attack_distanse)
					{
						msg = "active";
						onStunning();
						player.changeMana(-getMagicMana(type));
					}
					else
					{
						msg = "no distanse";
					}
				}
			}
			//crash
			if (type == "crash")
			{
				Debug.Log("crash");
				if (player.focusTarget == null || player.focusTarget ==gameObject)
				{
					msg = "no target";
				}
				else if (player.checkMana(getMagicMana(type))) 
				{
					msg = "no mana";
				}
				else
				{
					float dis = Vector3.Distance(transform.position,player.focusTarget.transform.position);
					if (dis < crash_attack_distanse)
					{
						msg = "active";
						onClash();
						player.changeMana(-getMagicMana(type));
					}
					else
					{
						msg = "no distanse";
					}
				}
			}
			//tankShield
			if (type == "tank_shield")
			{
				if (player.checkMana(getMagicMana(type)))
				{
					msg = "no mana";
				}
				else
				{
					msg = "active";
					onTankShield();
					player.changeMana(-getMagicMana(type));
				}
			}
			//fireball
			if (type == "fireball")
			{
				Debug.Log("fireball");
				if (player.focusTarget == null || player.focusTarget ==gameObject)
				{
					msg = "no target";
				}
				else if (player.checkMana(getMagicMana(type))) 
				{
					msg = "no mana";
				}
				else
				{
					float dis = Vector3.Distance(transform.position,player.focusTarget.transform.position);
					Debug.Log("dis="+dis);
					if (dis < fireball_distanse)
					{
						msg = "active";
						onFireBall();
						player.changeMana(-getMagicMana(type));
					}
					else
					{
						msg = "no distanse";
					}
				}
			}
			//freeze
			if (type == "freeze")
			{
				Debug.Log("freeze");
				if (player.focusTarget == null || player.focusTarget ==gameObject)
				{
					msg = "no target";
				}
				else if (player.checkMana(getMagicMana(type))) 
				{
					msg = "no mana";
				}
				else
				{
					float dis = Vector3.Distance(transform.position,player.focusTarget.transform.position);
					Debug.Log("dis="+dis);
					if (dis < freeze_distanse)
					{
						msg = "active";
						onFreezeBall();
						player.changeMana(-getMagicMana(type));
					}
					else
					{
						msg = "no distanse";
					}
				}
			}
			//take_attack
			if (type == "take_attack")
			{
				Debug.Log("take_attack");
				if (player.focusTarget == null || player.focusTarget ==gameObject)
				{
					msg = "no target";
				}
				else if (player.checkMana(getMagicMana(type))) 
				{
					msg = "no mana";
				}
				else
				{
					float dis = Vector3.Distance(transform.position,player.focusTarget.transform.position);
					Debug.Log("dis="+dis);
					if (dis < take_attack_distanse)
					{
						msg = "active";
						onTakeDamage();
						player.changeMana(-getMagicMana(type));
					}
					else
					{
						msg = "no distanse";
					}
				}
			}
		}
		//
		if (type == "wrath_of_nature")
		{
			Debug.Log("wrath_of_nature");
			if (player.focusTarget == null || player.focusTarget ==gameObject)
			{
				msg = "no target";
			}
			else if (player.checkMana(getMagicMana(type))) 
			{
				msg = "no mana";
			}
			else
			{
				float dis = Vector3.Distance(transform.position,player.focusTarget.transform.position);
				Debug.Log("dis="+dis);
				if (dis < wrath_of_nature_distanse)
				{
					msg = "active";
					onWrath();
					player.changeMana(-getMagicMana(type));
				}
				else
				{
					msg = "no distanse";
				}
			}
		}
		//nature_protection
		if (type == "nature_protection")
		{
			Debug.Log("nature_protection");
			if (player.focusTarget == null)
			{
				msg = "no target";
			}
			else if (player.checkMana(getMagicMana(type))) 
			{
				msg = "no mana";
			}
			else
			{
				float dis = Vector3.Distance(transform.position,player.focusTarget.transform.position);
				Debug.Log("dis="+dis);
				if (dis < nature_protection_distanse)
				{
					msg = "active";
					OnNatureProtection();
					player.changeMana(-getMagicMana(type));
				}
				else
				{
					msg = "no distanse";
				}
			}
		}
		//health
		if (type == "health")
		{
			Debug.Log("health");
			if (player.focusTarget == null)
			{
				msg = "no target";
			}
			else if (player.checkMana(getMagicMana(type))) 
			{
				msg = "no mana";
			}
			else
			{
				float dis = Vector3.Distance(transform.position,player.focusTarget.transform.position);
				Debug.Log("dis="+dis);
				if (dis < nature_protection_distanse)
				{
					msg = "active";
					onHealth();
					player.changeMana(-getMagicMana(type));
				}
				else
				{
					msg = "no distanse";
				}
			}
		}
		Debug.Log(msg);
		return msg;
	}


	//health
	void onHealth()
	{
		SendMessage("BlockMove");
		animation.CrossFade(player.cast[1]);
		player.currentAnimation = player.cast[1];
		if (player.focusTarget != gameObject) transform.rotation = Quaternion.LookRotation(player.focusTarget.transform.position - transform.position);
		Invoke("onHealthCast",animation[player.cast[1]].length/1.2f);

	}
	void onHealthCast()
	{
		SendMessage("UnblockMove");
		player.animation.CrossFade(player.idle);
		player.currentAnimation = player.idle;
		if (player.focusTarget)
		{
			PlayerAI pl = (PlayerAI)player.focusTarget.GetComponent("PlayerAI");
			pl.photonView.RPC("onHealthPlayer",PhotonTargets.AllBuffered,health_value);
			pl.magic.photonView.RPC("onSetMagicEffect",PhotonTargets.All,true,"health_effect");
		}
	}

	//nature protection
	void OnNatureProtection()
	{
		SendMessage("BlockMove");
		animation.CrossFade(player.cast[1]);
		player.currentAnimation = player.cast[1];
		transform.rotation = Quaternion.LookRotation(player.focusTarget.transform.position - transform.position);
		Invoke("OnNatureProtectionCast",animation[player.cast[1]].length/1.2f);
	}

	void OnNatureProtectionCast()
	{
		SendMessage("UnblockMove");
		player.animation.CrossFade(player.idle);
		player.currentAnimation = player.idle;
		if (player.focusTarget)
		{
			PlayerAI ai = (PlayerAI)player.focusTarget.GetComponent("PlayerAI");
			ai.photonView.RPC("onSetNatureProtection",PhotonTargets.AllBuffered);
		}
	}
	[RPC]
	void onSetNatureProtection(PhotonMessageInfo info)
	{	
		PlayerAI pl = (PlayerAI)gameObject.GetComponent("PlayerAI");
		if (baffes.Contains("nature_protection"))
		{
			StopCoroutine("nature_protection");
			pl.changeParameter("defense",-nature_protection_value,null);
		}

		pl.changeParameter("defense",nature_protection_value,null);
		setBaff("nature_protection","onEndNatureProtection");
		StartCoroutine("onEndNatureProtection");
	}
	IEnumerator onEndNatureProtection()
	{
		yield return new WaitForSeconds(nature_protection_time);
		player.changeParameter("defense",-nature_protection_value,null);
		endBaff("nature_protection");
	}
	//wrath
	void onWrath()
	{
		SendMessage("BlockMove");
		animation.CrossFade(player.cast[2]);
		player.currentAnimation = player.cast[2];
		transform.rotation = Quaternion.LookRotation(player.focusTarget.transform.position - transform.position);
		Invoke("onWrathCast",animation[player.cast[2]].length/1.2f);
	}
	void onWrathCast()
	{
		SendMessage("UnblockMove");
		player.animation.CrossFade(player.idle);
		player.currentAnimation = player.idle;
		photonView.RPC("setWrath",PhotonTargets.AllBuffered,player.focusTarget.transform.position,transform.position);
	}
	
	[RPC]
	void setWrath(Vector3 pos,Vector3 point,PhotonMessageInfo info)
	{
		GameObject wrath = (GameObject)Instantiate(Resources.Load("magic/wrath"));
		wrath.transform.position = point;
		iTween.MoveTo(wrath,iTween.Hash("position",pos,"easetype","linear","time",1,"oncomplete","onDamageWrath","oncompletetarget",gameObject,"oncompleteparams",wrath));
	}
	void onDamageWrath(GameObject wrath)
	{
		GameObject.Destroy(wrath);
		if (photonView.isMine)
		{
			player.onSetMagicDamage(Global.calculateDamage(wrath_of_nature_damage + player.intellect_range*4,1,1));
		}
	}


	//take damage
	void onTakeDamage()
	{
		SendMessage("BlockMove");
		animation.CrossFade(player.cast[1]);
		player.currentAnimation = player.cast[1];
		Invoke("onTakeDamageEnd",animation[player.cast[1]].length);
	}
	void onTakeDamageEnd()
	{
		SendMessage("UnblockMove");
		player.animation.CrossFade(player.idle);
		player.currentAnimation = player.idle;
		PlayerAI pl = (PlayerAI)player.focusTarget.GetComponent("PlayerAI");
		pl.photonView.RPC("takeAttackPlayer",PhotonTargets.AllBuffered,take_attack_time,take_attack_valuse);
		pl.magic.photonView.RPC("onSetMagicEffect",PhotonTargets.All,true,"attacktaking_effect");
	}

	//freeze
	void onFreezeBall()
	{
		SendMessage("BlockMove");
		animation.CrossFade(player.cast[5]);
		player.currentAnimation = player.cast[5];
		transform.rotation = Quaternion.LookRotation(player.focusTarget.transform.position - transform.position);
		Invoke("onFreezeBallCast",animation[player.cast[5]].length/1.6f);
		
	}
	void onFreezeBallCast()
	{
		SendMessage("UnblockMove");
		player.animation.CrossFade(player.idle);
		player.currentAnimation = player.idle;
		photonView.RPC("setFreezeBall",PhotonTargets.AllBuffered,player.focusTarget.transform.position,player.attackPoint.transform.position);
	}
	
	[RPC]
	void setFreezeBall(Vector3 pos,Vector3 point,PhotonMessageInfo info)
	{
		GameObject freezeball = (GameObject)Instantiate(Resources.Load("magic/freezeBall"));
		freezeball.transform.position = point;
		iTween.MoveTo(freezeball,iTween.Hash("position",pos,"easetype","linear","time",1,"oncomplete","onDamageFreezeBall","oncompletetarget",gameObject,"oncompleteparams",freezeball));
	}
	void onDamageFreezeBall(GameObject freezeball)
	{
		GameObject.Destroy(freezeball);
		if (photonView.isMine)
		{
			player.onSetMagicDamage(Global.calculateDamage(freeze_damage + player.intellect_range*4,1,1));
			if (player.focusTarget)
			{
				PlayerAI pl = (PlayerAI)player.focusTarget.GetComponent("PlayerAI");
				pl.photonView.RPC("freezePlayer",PhotonTargets.AllBuffered,freeze_time,freeze_value);
			}
		}
	}


	//fire ball
	void onFireBall()
	{
		SendMessage("BlockMove");
		animation.CrossFade(player.cast[5]);
		player.currentAnimation = player.cast[5];
		transform.rotation = Quaternion.LookRotation(player.focusTarget.transform.position - transform.position);
		Invoke("onFireBallCast",animation[player.cast[5]].length/1.6f);

	}
	void onFireBallCast()
	{
		SendMessage("UnblockMove");
		player.animation.CrossFade(player.idle);
		player.currentAnimation = player.idle;
		photonView.RPC("setFireBall",PhotonTargets.AllBuffered,player.focusTarget.transform.position,player.attackPoint.transform.position);
	}

	[RPC]
	void setFireBall(Vector3 pos,Vector3 point,PhotonMessageInfo info)
	{
		GameObject fireball = (GameObject)Instantiate(Resources.Load("magic/fireBall"));
		fireball.transform.position = point;
		iTween.MoveTo(fireball,iTween.Hash("position",pos,"easetype","linear","time",1,"oncomplete","onDamageFireBall","oncompletetarget",gameObject,"oncompleteparams",fireball));
	}
	void onDamageFireBall(GameObject fireball)
	{
		GameObject.Destroy(fireball);
		if (photonView.isMine)
		{
			player.onSetMagicDamage(Global.calculateDamage(fireball_damage + player.intellect_range*4,1,1));
		}
	}


	//tank_shield
	void onTankShield()
	{
		player.photonView.RPC("changeParameter",PhotonTargets.All,"defense",tank_shield_defense);
		setBaff("tank_shield","onTankShieldEnd");
		StartCoroutine("onTankShieldEnd");
	}
	IEnumerator onTankShieldEnd()
	{
		yield return new WaitForSeconds(tank_shield_time);
		player.photonView.RPC("changeParameter",PhotonTargets.All,"defense",-tank_shield_defense);
		endBaff("tank_shield");
	}

	//crash
	void onClash()
	{
		SendMessage("BlockMove");
		animation.CrossFade(player.cast[0]);
		player.currentAnimation = player.cast[0];
		Invoke("onClashEnd",animation[player.cast[0]].length/1.4f);
		player.isFelowToTarget = true;
	}
	void onClashEnd()
	{
		player.onSetMagicDamage(Global.calculateDamage(player.attack_range+player.defense_range,player.min_damage_range,player.max_damage_range)*crash_attack_multiply);
		SendMessage("UnblockMove");
		player.isFelowToTarget = false;
		player.animation.CrossFade(player.idle);
		player.currentAnimation = player.idle;
	}

	//stunning
	void onStunning()
	{
		SendMessage("BlockMove");
		animation.CrossFade(player.cast[0]);
		player.currentAnimation = player.cast[0];
		Invoke("onEndSunning",animation[player.cast[0]].length/1.4f);
		player.isFelowToTarget = true;
	}
	void onEndSunning()
	{
		if (player.focusTarget)
		{
			PlayerAI pl = (PlayerAI)player.focusTarget.GetComponent("PlayerAI");
			pl.photonView.RPC("stunPlayer",PhotonTargets.AllBuffered,stunning_stun_time);
			player.onSetMagicDamage(Global.calculateDamage(player.attack_range,player.min_damage_range,player.max_damage_range)*stunning_attack_multiply);
			SendMessage("UnblockMove");
			player.isFelowToTarget = false;
			player.animation.CrossFade(player.idle);
			player.currentAnimation = player.idle;
			//pl.magic.setBaff("stunning","onStunEnded");
			//StartCoroutine("onStunEnded");
		}
	}
	IEnumerator onStunEnded()
	{
		yield return new WaitForSeconds(stunning_stun_time);
		endBaff("stunning");
	}

	//spurt
	void onSpurt()
	{
		photonView.RPC("onSetMagicEffect",PhotonTargets.AllBuffered,true,"rage_effect");
		player.photonView.RPC("changeParameter",PhotonTargets.AllBuffered,"move_speed",spurt_range);
		setBaff("spurt","onSpurtEnded");
		onSpurtEnd();
	}
	void onSpurtEnd()
	{
		StartCoroutine("onSpurtEnded");
	}
	IEnumerator onSpurtEnded()
	{
		yield return new WaitForSeconds(spurt_time);
		player.photonView.RPC("changeParameter",PhotonTargets.AllBuffered,"move_speed",1/spurt_range);
		endBaff("spurt");
	}

	//critical_attack
	void onCriticalAttack()
	{
		SendMessage("BlockMove");
		animation.CrossFade(player.cast[0]);
		animation[player.cast[0]].speed = 2;
		player.currentAnimation = player.cast[0];
		Invoke("onEndCriticalAttack",animation[player.cast[0]].length/1.4f/2);
		player.isFelowToTarget = true;

	}
	void onEndCriticalAttack()
	{
		player.onSetMagicDamage(Global.calculateDamage(player.attack_range,player.min_damage_range,player.max_damage_range)*critical_attack_multiply);
		SendMessage("UnblockMove");
		player.isFelowToTarget = false;
		player.animation.CrossFade(player.idle);
		player.currentAnimation = player.idle;
	}


	//rage
	void onRage()
	{
		photonView.RPC("onSetMagicEffect",PhotonTargets.AllBuffered,true,"rage_effect");
		player.photonView.RPC("changeParameter",PhotonTargets.AllBuffered,"attack_speed",rage_attack_speed_range);
		player.photonView.RPC("changeParameter",PhotonTargets.AllBuffered,"attack",rage_attack_range);
		setBaff("rage","onRageEnd");
		StartCoroutine("onRageEnd");
	}
	IEnumerator onRageEnd()
	{
		yield return new WaitForSeconds(rage_time);
		photonView.RPC("onRageEnded",PhotonTargets.AllBuffered);
	}
	[RPC]
	void onRageEnded(PhotonMessageInfo info)
	{
		player.changeParameter("attack_speed",1/rage_attack_speed_range,null);
		player.changeParameter("attack",-rage_attack_range,null);
		endBaff("rage");
	}
	//



	[RPC]
	void onSetMagicEffect(bool me,string type,PhotonMessageInfo info)
	{
		GameObject effect = (GameObject)Instantiate(Resources.Load("magic/"+type));
		if (me)
		{
			effect.transform.position = gameObject.transform.position;
			effect.transform.parent = gameObject.transform;
		}
		else
		{
			effect.transform.position = player.focusTarget.transform.position;
			effect.transform.parent = player.focusTarget.transform;
		}

	}
	//cooldown
	public int getMagicCoolDown(string type)
	{
		int cooldown = 0;
		//bersek
		if (type == "rage") cooldown = 25;
		if (type == "critical_attack") cooldown = 20;
		if (type == "spurt") cooldown = 30;
		//tank
		if (type == "stunning") cooldown = 25;
		if (type == "crash") cooldown = 10;
		if (type == "tank_shield") cooldown = 60;
		//mage
		if (type == "fireball") cooldown = 5;
		if (type == "freeze") cooldown = 20;
		if (type == "take_attack") cooldown = 30;
		//priest
		if (type == "wrath_of_nature") cooldown = 7;
		if (type == "nature_protection") cooldown = 10;
		if (type == "health") cooldown = 10;
		return cooldown;
	}
	public float getMagicMana(string type)
	{
		float mana = 10000000;
		//bersek
		if (type == "rage") mana = 15;
		if (type == "critical_attack") mana = 10;
		if (type == "spurt") mana = 20;
		//tank
		if (type == "stunning") mana = 25;
		if (type == "crash") mana = 20;
		if (type == "tank_shield") mana = 30;
		//mage
		if (type == "fireball") mana = 15;
		if (type == "freeze") mana = 40;
		if (type == "take_attack") mana = 35;
		//priest
		if (type == "wrath_of_nature") mana = 25;
		if (type == "nature_protection") mana = 35;
		if (type == "health") mana = 60;
		return mana;
	}

	public void setBaff(string type,string cor)
	{
		if (photonView.isMine)
		{
			if (baffes.Contains(type))
			{
				baffes.Remove(type);
				if (type == "rage")
				{
					player.photonView.RPC("changeParameter",PhotonTargets.AllBuffered,"attack_speed",1/rage_attack_speed_range);
					player.photonView.RPC("changeParameter",PhotonTargets.AllBuffered,"attack",-rage_attack_range);
				}
				if (type == "spurt")
				{
					player.photonView.RPC("changeParameter",PhotonTargets.AllBuffered,"move_speed",1/spurt_range);
				}
				if (type == "stunning")
				{
					//player.unstunPlayer();
				}
				if (type == "freeze")
				{
					//player.changeParameter("move_speed",1+val,null);
				}
				if (type == "tank_shield")
				{
					player.photonView.RPC("changeParameter",PhotonTargets.All,"defense",-tank_shield_defense);
				}
				StartCoroutine(onStopCor(cor));
			}
			baffes.Add(type);
			player.gui.setBaffIcon(baffes);
		}
	}

	IEnumerator onStopCor(string elum)
	{
		yield return new WaitForSeconds(0.2f);
		StopCoroutine(elum);
		StartCoroutine(elum);
	}

	public void endBaff(string type)
	{
		if (photonView.isMine)
		{
			if (baffes.Contains(type)) baffes.Remove(type);
			player.gui.setBaffIcon(baffes);
		}
	}

	IEnumerator onSimpleEnd()
	{
		yield return new WaitForSeconds(0.2f);
	}
}
